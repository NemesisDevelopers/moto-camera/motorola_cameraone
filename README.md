# Motorola Moto Camera One

<center><img src="https://telegra.ph/file/cf43da1a6481e7c094ff6.jpg"/></center>

**Compatible Devices:**

- Droid Maxx (obake-maxx)
- Droid Maxx 2 (lux)
- Droid Mini (obakem)
- Droid Turbo (quark)
- Droid Turbo 2 (kinzie)
- Droid Ultra (obake)
- Moto E (condor)
- Moto E² (otus)
- Moto E² LTE (surnia)
- Moto E⁴ (Qualcomm) (perry/sperry)
- Moto E⁴ Plus (Qualcomm) (owens)
- Moto E⁵ (nora)
- Moto E⁵ Plus (rhannah/hannah/ahannah)
- Moto E⁵ Play (rjames/james)
- Moto E⁵ Go (rjames)
- Moto E⁵ Supra (hannah)
- Moto E⁵ Cruise (james)
- Moto E⁵ Play Go (pettyl)
- Moto G (falcon)
- Moto G Forte (falcon)
- Moto G Google Play Edition (falcon)
- Moto G Ferrari Edition (falcon)
- Moto G LTE (peregrine)
- Moto G² (titan)
- Moto G² LTE (thea)
- Moto G³ (osprey)
- Moto G³ Turbo Edition (merlin)
- Moto G⁴ (athene)
- Moto G⁴ Plus (athene)
- Moto G⁴ Play (harpia)
- Moto G⁵ (cedric)
- Moto G⁵ Plus (potter)
- Moto G⁵ˢ (montana)
- Moto G⁶ Play (aljeter/jeter)
- Moto G⁶ Forge (aljeter)
- Moto Green Pomelo (montana)
- Moto Maxx (quark)
- Moto X (ghost)
- Moto X² (victara)
- Moto X² Pro (shamu)
- Moto X³ Style (clark)
- Moto X³ Pure Edition (clark)
- Moto X³ Play (lux)
- Moto X³ Force (kinzie)
- Moto Z (griffin)
- Moto Z Droid (griffin)
- Moto Z Force Droid (sheridan)
- Moto Z Play (addison)
- Moto Z Play Droid (addison)
- Moto Z² Play (albus)
- Motorola E⁵ Plus (ahannah)


**How to add it in your tree**

To clone:

`git clone https://gitlab.com/NemesisDevelopers/moto-camera/motorola_cameraone.git -b eleven-arm64 packages/apps/MotCamera`

`git clone https://gitlab.com/NemesisDevelopers/moto-camera/motorola_cameraone.git -b ten packages/apps/MotCamera-overlay`

`git clone https://gitlab.com/NemesisDevelopers/motorola/motorola_motosignatureapp.git -b eleven packages/apps/MotoSignatureApp`

Add this in your dependencies:

```
 {
   "repository": "motorola_cameraone",
   "target_path": "packages/apps/MotCamera",
   "branch": "eleven-arm64",
   "remote": "moto-camera"
 }
```
Add this in your device.mk or common.mk:

```
# Moto Camera
PRODUCT_PACKAGES += \
    MotCamera
```

# [Download & info](https://telegra.ph/List-N-1-01-03)

 Copyright © 2020-2021 Nemesis Team
